# Try Linux

Try Linux is a website that's meant to be a friendly place to start for the Free & Open Source Software and Linux curious. Try Linux is a site maintained by [ThinkPrivacy](https://www.thinkprivacy.io) and contributors.

## Copying & Reuse

All original copy (written text) and images on this site are licensed under the Creative Commons Attribution-ShareAlike 4.0, unless otherwise noted.

Any unique source code is free software, licensed under the terms of the Apache License v2.0, unless otherwise noted. Any non-unique elements (such as bundled JavaScript resources) are licensed under the terms of their accompanying licenses, or stated within their source code.

## Local Development

This site is built using the static site generator [Jekyll](https://jekyllrb.com/) (visit their site for info on how to set that up), but you will need to have it installed to hack on this site. Once you do so you can load the site with:

	bundle exec jekyll serve

## Contributing

Contributions are obviously welcome! If you would like to contribute to this project, please have [read this](/CONTRIBUTING.md) regarding contributions.

Alternatively, if you would like to support development by making a donation you can do so [here](https://www.thinkprivacy.io/donate.html).

## Source

Try Linux is a fork of [ComputeFreely](https://computefreely.org/), an open source website for which we based ours on. After making multiple pull requests that went unanswered, and seeing others do the same, we deemed the project inactive and forked it into the site you're visiting now.

Our plan is to continue to develop and expand the site beyond its original form and become something new altogether.
